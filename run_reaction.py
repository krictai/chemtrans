import os
import glob
import warnings
import time
import logging
import numpy as np
import math
import time
import argparse
from multiprocessing import Process, Queue

from rdkit import RDLogger
from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit import DataStructs
from rdkit.Chem import Descriptors
from moses.metrics import SA
from tqdm import tqdm
import modification_utils

def read_chemical_transformation_rules(filename):
    rules = {}
    cnt = 1
    with open(filename, 'r') as fp:
        for line in fp:
            cnt+=1
            sptlist = line.strip().split('\t')
            pattern_smiles = sptlist[0].strip()
            replace_smiles = sptlist[1].strip()
            template_smiles = sptlist[2].strip()
            reaction_type = sptlist[3].strip()

            rules[(pattern_smiles, replace_smiles, template_smiles, reaction_type)]=1

    return rules

def read_input_smiles(input_file):
    smiles_list = []
    with open(input_file, 'r') as fp:
        for line in fp:
            input_mol = Chem.MolFromSmiles(line.strip())
            smiles = Chem.MolToSmiles(input_mol)
            smiles_list.append(smiles)
    return smiles_list

def do_chemical_transformation(input_smiles_list, reaction_rule_info, output):
    total_smiles = {}
    s1 = 0.0
    s2 = 0.0
    s3 = 0.0
    e1 = 0.0
    e2 = 0.0
    e3 = 0.0
    
    for i in tqdm(range(0, len(input_smiles_list)),mininterval=1):
        input_smiles = input_smiles_list[i]
        cnt = 0
        input_mol = Chem.MolFromSmiles(input_smiles)
        input_mol_atom_num = len(input_mol.GetAtoms())
        mol_cut_off = int(input_mol_atom_num*0.2)
        
        key_list = list(reaction_rule_info.keys())        
        
        for j in tqdm(range(0, len(key_list)),mininterval=1):
            each_rule = key_list[j]
            cnt+=1
            pattern_smiles = each_rule[0]
            replace_smiles = each_rule[1]
            template_smiles = each_rule[2]
            reaction_type = each_rule[3]

            pattern_mol = Chem.MolFromSmiles(pattern_smiles)
            replace_mol = Chem.MolFromSmiles(replace_smiles)
            
            if pattern_mol != None and replace_mol != None:
                pattern_mol_atom_num = len(pattern_mol.GetAtoms())
                replace_mol_atom_num = len(replace_mol.GetAtoms())
                
                if reaction_type == 'ADD':
                    if pattern_mol_atom_num <= mol_cut_off:
                        s = time.time()
                        try:
                            results = modification_utils.add_substructures(input_mol, replace_mol, pattern_mol)
                        except:
                            continue
                        e = time.time()
                        s1+=(e-s)
                        for smiles in results:
                            total_smiles[smiles] = 1
                            
                if reaction_type == 'REMOVE':
                    if pattern_mol_atom_num <= mol_cut_off:
                        s = time.time()
                        try:
                            results = modification_utils.remove_substructures(input_mol, pattern_mol)
                        except:
                            continue
                        e = time.time()
                        s2+=(e-s)
                        for smiles in results:
                            total_smiles[smiles] = 1
                            
                if reaction_type == 'REPLACE':
                    if abs(pattern_mol_atom_num-replace_mol_atom_num) <= mol_cut_off:
                        template_mol = Chem.MolFromSmiles(template_smiles)
                        if template_mol != None:
                            try:
                                s = time.time()
                                results = modification_utils.replace_substructures(input_mol, pattern_mol, replace_mol, template_mol)
                                e = time.time()
                                s3+=(e-s)
                            except:
                                continue

                            for smiles in results:
                                total_smiles[smiles] = 1
                            
    output.put(total_smiles)
    
def split_data(l, n):
    size = int(math.ceil(len(l)/float(n)))
    data_list = []
    for i in range(0, len(l), size):
        data_list.append(l[i:i+size])
    return data_list

def run_transformation(input_file, rule_filename, n_cpu=1):
    input_smiles_list = read_input_smiles(input_file)
    transformation_pattern_info = read_chemical_transformation_rules(rule_filename)
    
    if len(input_smiles_list) < n_cpu:
        n_cpu = len(input_smiles_list)
    
    output_list = []
    for i in range(n_cpu):
        output_list.append(Queue())
        
    procs = []
    proc_cnt = 0
    data_size = len(input_smiles_list)/n_cpu

    for tmp_smile_list in split_data(input_smiles_list, n_cpu):
        procs.append(Process(target=do_chemical_transformation, args=(tmp_smile_list, transformation_pattern_info, output_list[proc_cnt])))
        proc_cnt+=1
        
    for p in procs:
        p.start()
        
    results = []
    for i in range(n_cpu):
        results.append(output_list[i].get())
        output_list[i].close()
                     
    for p in procs:
        p.join()    
        
    final_results = merge_results(results)
    return final_results

def merge_results(results):
    final_results = {}
    for each_result_set in results:
        for each_smiles in each_result_set:
            final_results[each_smiles] = 1
    return final_results

def run_filtering(input_file, output_file, filtered_output_file, threshold):
    target_atoms = ['C', 'N', 'O', 'S', 'F', 'Cl', 'Br', 'H']
    
    max_atom = 70
    max_weight = 600
    top_n_percent = 0.3
    
    input_mol_list = []
    generative_mol_list = []
    generative_smiles_list = []
    final_mol_list = []
    final_mol_sa_list = []
    with open(input_file, 'r') as fp:
        for line in fp:
            input_mol = Chem.MolFromSmiles(line.strip())
            input_mol_list.append(input_mol)
                
    with open(output_file, 'r') as fp:
        for line in fp:
            tmp_mol = Chem.MolFromSmiles(line.strip())
            generative_mol_list.append(tmp_mol)
            generative_smiles_list.append(line.strip())
    
    for i in tqdm(range(0, len(input_mol_list)),mininterval=1):
        try:
            each_input_mol = input_mol_list[i]
            fp1 = AllChem.GetMorganFingerprint(each_input_mol,2)
        except:
            continue

        for j in tqdm(range(0, len(generative_mol_list)),mininterval=1):
            try:
                each_tmp_mol = generative_mol_list[j]
                fp2 = AllChem.GetMorganFingerprint(each_tmp_mol,2)
            except:
                continue
            
            sim = DataStructs.DiceSimilarity(fp1, fp2)
            
            if sim < threshold:
                continue
                
            if Descriptors.ExactMolWt(each_tmp_mol) > max_weight:
                continue 
                
            if len(each_tmp_mol.GetAtoms()) > max_atom:
                continue
                
            flag = True
            for each_atom in each_tmp_mol.GetAtoms():
                if each_atom.GetSymbol() not in target_atoms:
                    flag = False

            if flag == False:
                continue
            
            smiles = generative_smiles_list[j]
            final_mol_list.append(smiles)
    
    top_n = int(len(final_mol_list)*top_n_percent)
    
    final_mol_list = list(set(final_mol_list))
    for smiles in final_mol_list:
        tmp_mol = Chem.MolFromSmiles(smiles.strip())
        sa_score = SA(tmp_mol)
        final_mol_sa_list.append([sa_score, smiles])
        
    final_mol_sa_list.sort()
    final_mol_sa_list = final_mol_sa_list[0:top_n]
    
    with open(filtered_output_file, 'w') as fp:
        for data in final_mol_sa_list:
            smiles = data[1].strip()
            fp.write('%s\n'%(smiles))
            
    return

def argument_parser():
    parser = argparse.ArgumentParser()    
    parser.add_argument('-o', '--output_dir', required=True, help="Output directory")
    parser.add_argument('-i', '--smiles_file', required=True, help="Input smiles file") 
    return parser

def main():
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    lg = RDLogger.logger()
    lg.setLevel(RDLogger.CRITICAL)
    
    warnings.filterwarnings('ignore')
    start = time.time()
    logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
    
    parser = argument_parser()
    options = parser.parse_args()
    input_file = options.smiles_file
    output_dir = options.output_dir
    
    try:
        os.mkdir(output_dir)
    except:
        pass
    
    initial_input_file = input_file
    basename = os.path.basename(input_file).split('.')[0].strip()
    
    chemical_transformation_rule_file = './data/Chemical_transformation_patterns.txt'
        
    n_step = 1
    similarity_threshold = 0.8
    
    step_cnt = 0
    while True:
        if step_cnt == n_step:
            break
        
        try:
            os.mkdir(output_dir)
        except:
            pass
        
        output_file = output_dir+'/raw_smiles.smi'
        filtered_output_file = output_dir+'/filtered_smiles.smi'
        
        total_smiles = run_transformation(input_file, chemical_transformation_rule_file)
    
        fp = open(output_file, 'w')
        for smiles in total_smiles:
            fp.write('%s\n'%(smiles))
        fp.close()
        
        run_filtering(initial_input_file, output_file, filtered_output_file, similarity_threshold)
        input_file = filtered_output_file
        step_cnt+=1

    logging.info(time.strftime("Elapsed time %H:%M:%S", time.gmtime(time.time() - start)))

if __name__ == '__main__':
    main()
    




