# *In silico* chemical transformation module

## Procedure

**Note**: 
This source code was developed in Linux, and has been tested in Ubuntu 16.04 with Python 3.6.

1. Clone the repository

        git clone https://bitbucket.org/krictai/chemtrans.git

2. Create and activate a conda environment

        conda env create -f environment.yml
        conda activate chemtrans
            

## Example

- Run *in silico* chemical transformation module

        python run_reaction.py -i ./examples/test_input.smi -o ./output 

